<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    //
    protected $table = "order_details";
    protected $fillable = [
        'client_id','product_id','price','quantity','discount','total','product_sku','size',
        'color','shipdate','billdate'];


    
}
