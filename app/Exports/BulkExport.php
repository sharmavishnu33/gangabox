<?php

namespace App\Exports;

use App\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use App\ProductVariant;
use App\ProductImage;
use App\Exports\BulkExport;

class BulkExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
   public function headings(): array
    {
        return [
            'Id',
            'Name',
            'Description',
            'Product_Id',
            'Product_video_link',
            'Created_By',
            'Status',
            'createdAt',
            'updatedAt',
        ];
    }
    public function collection()
    {
        $products = Product::with('productVariant.productImage')->get();
        dd($products);
        return $products;

         //  $products = Product::with('productVariant.productImage')->get();
         
         // return view('admin.product_list', compact('products'));

        /*you can use condition in query to get required result
         return Bulk::query()->whereRaw('id > 5');*/
    }
    public function map($bulk): array
    {
        return [
            // $bulk->id,
            // $bulk->name,
            // $bulk->p_status,
            // $bulk->p_id,
            // Date::dateTimeToExcel($bulk->created_at),
            // Date::dateTimeToExcel($bulk->updated_at),
        ];
    }

}

