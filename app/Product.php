<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $table = 'products';

	protected $fillable = ['name', 'description', 'category_id','p_id', 'p_status'];

	public function productVariant()
	{
		return $this->hasOne('App\ProductVariant', 'product_id', 'id');
	}
}
