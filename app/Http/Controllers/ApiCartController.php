<?php

namespace App\Http\Controllers;

use App\Cart;
use App\GuestUserInfo;
use App\OrderDetails;
use App\Product;
use App\User;
use Carbon\CarbonTimeZone;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Validator,Redirect,Response;
use Storage;
use DB;

class ApiCartController extends Controller
{

    public function cartAdd(Request $request)
    {

        try {

            $rules = [
                'product_name' => 'required',
                'product_price' => 'required',
                'description' => 'required',
                'product_image' => 'required',
                'quantity' => 'required',
                'product_color' => 'required',
                'product_size' => 'required',
//                'sub_total_price' => 'required',
//                'shipping_price' => 'required',
//                'total_price' => 'required',
                'device_token' => 'required',
                'product_id' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {
//                $response = ['success' => false, 'errors' => $validator->errors()->all()];
//
              $response = ['success' => false, 'message' => 'Cart added failed.'];
            } else {
                $product = Product::find($requestData['product_id']);
                if($product) {
                    $data = array('product_name' => $requestData['product_name'],
                        'product_price' => $requestData['product_price'],
                        'description' => $requestData['description'],
                        'product_image' => $requestData['product_image'],
                        'quantity' => $requestData['quantity'],
                        'product_color' => $requestData['product_color'],
                        'product_size' => $requestData['product_size'],
//                        'sub_total_price' => $requestData['sub_total_price'],
//                        'shipping_price' => $requestData['shipping_price'],
//                        'total_price' => $requestData['total_price'],
                        'device_token' => $requestData['device_token'],
                        'product_id' => $requestData['product_id']);
                    $cartData = Cart::create($data);
                    $response = ['success' => true, 'message' => 'Cart added successfully.'];
                }
                else
                {
                    $response = ['success' => true, 'message' => 'Cart added failed.'];
                }

            }

            return Response::json($response,200);


        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);

        }
    }

    public function cartlist(Request $request)
    {

        try {

            $rules = [
                'device_token' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {

                $response = ['success' => false, 'message' => 'Cart listing failed.'];
            } else {
                $cartList = Cart::where('device_token',$request->device_token)->get();

                $response = ['success' => true, 'message' => 'Cart list successfully.','data' => $cartList];

            }

            return Response::json($response,200);


        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);

        }
    }

    public function clientInformation(Request $request)
    {
        try {
            $rules = [
                'email' => 'required|regex:/(.+)@(.+)\.(.+)/i',
                'name' => 'required',
                'state' => 'required',
                'phone' => 'required',
                'postal_code' => 'required',
                'street' => 'required',
                'street_number' => 'required',
                'reference' => 'required',
                'device_token' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {

                $response = ['success' => false, 'message' => 'Client info failed.'];
            } else {
                $data = array(
                    'email' =>$requestData['email'],
                    'name' =>$requestData['name'],
                    'state' =>$requestData['state'],
                    'mobile' =>$requestData['phone'],
                    'postal_code' =>$requestData['postal_code'],
                    'street' =>$requestData['street'],
                    'street_number' =>$requestData['street_number'],
                    'reference' =>$requestData['reference'],
                    'device_token' =>$requestData['device_token']);
                $clientExist = GuestUserInfo::where('device_token',$requestData['device_token'])->first();
                $clientData ='';
                if($clientExist)
                {
                    GuestUserInfo::where('device_token',$requestData['device_token'])->update($data);
                    $clientData = GuestUserInfo::where('device_token',$requestData['device_token'])->first();
                }else {
                    $clientData = GuestUserInfo::create($data);
                }
                $response = ['success' => true, 'message' => 'Client information save successfully.','data'=>$clientData];
            }
            return Response::json($response,200);
        }
        catch (Exception $e) {
            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);
        }

    }

    public function editCart(Request $request)
    {
        try {

            $rules = [
                'cart_id' => 'required',
                'product_name' => 'required',
                'product_price' => 'required',
                'description' => 'required',
                'product_image' => 'required',
                'quantity' => 'required',
                'product_color' => 'required',
                'product_size' => 'required',
//                'sub_total_price' => 'required',
//                'shipping_price' => 'required',
//                'total_price' => 'required',
                'device_token' => 'required',
                'product_id' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {
//                $response = ['success' => false, 'errors' => $validator->errors()->all()];

                $response = ['success' => false, 'message' => 'Cart edit failed.'];
            } else {
                $product = Product::find($requestData['product_id']);
                $cart = Cart::find($requestData['cart_id']);
                if($cart) {
                    if ($product) {
                        $data = array('product_name' => $requestData['product_name'],
                            'product_price' => $requestData['product_price'],
                            'description' => $requestData['description'],
                            'product_image' => $requestData['product_image'],
                            'quantity' => $requestData['quantity'],
                            'product_color' => $requestData['product_color'],
                            'product_size' => $requestData['product_size'],
//                            'sub_total_price' => $requestData['sub_total_price'],
//                            'shipping_price' => $requestData['shipping_price'],
//                            'total_price' => $requestData['total_price'],
                            'device_token' => $requestData['device_token'],
                            'product_id' => $requestData['product_id']);
                        $cartData = Cart::create($data);
                        $response = ['success' => true, 'message' => 'Cart edit successfully.'];
                    } else {
                        $response = ['success' => true, 'message' => 'Cart edit failed.'];
                    }
                }
                else
                {
                    $response = ['success' => true, 'message' => 'Cart edit failed.'];
                }

            }

            return Response::json($response,200);


        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);

        }
    }

    public function deleteCart(Request $request)
    {
        try {

            $rules = [
                'cart_id' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {
//                $response = ['success' => false, 'errors' => $validator->errors()->all()];

                $response = ['success' => false, 'message' => 'Cart delete failed.'];
            } else {
                $cart = Cart::find($requestData['cart_id']);
                if($cart) {
                    Cart::where('id',$requestData['cart_id'])->delete();
                    $response = ['success' => true, 'message' => 'Cart delete successfully.'];
                }
                else
                {
                    $response = ['success' => false, 'message' => 'Cart delete failed.'];
                }

            }

            return Response::json($response,200);


        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);

        }
    }

    public function addOrder(Request $request)
    {

        try {

            $rules = [
                'client_id' => 'required',
                'product_id' => 'required',
                'price' => 'required',
                'quantity' => 'required',
                'discount' => 'required',
                'total' => 'required',
                'product_sku' => 'required',
                'size' => 'required',
                'color' => 'required',
                'shipdate' => 'required',
                'billdate' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {
//                $response = ['success' => false, 'errors' => $validator->errors()->all()];

                $response = ['success' => false, 'message' => 'Order add failed.'];
            } else {
                $data = array('product_id' =>$requestData['product_id'],'price' =>$requestData['price'],'quantity' =>$requestData['quantity'],
                    'discount' =>$requestData['discount'],'total' =>$requestData['total'],'product_sku' =>$requestData['product_sku'],'size' =>$requestData['size']
                ,'color' =>$requestData['color'],'shipdate' =>$requestData['shipdate'],'billdate' =>$requestData['billdate']);

                $orderData = OrderDetails::create($data);
                    $response = ['success' => true, 'message' => 'Order delete successfully.'];

            }

            return Response::json($response,200);


        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);

        }

    }



}
