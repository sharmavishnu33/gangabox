<?php

namespace App\Http\Controllers;

use GrahamCampbell\Binput\Facades\Binput;
use Illuminate\Http\Request;
use Validator,Redirect,Response;
use App\Http\Controllers\ProductVariantController;
use App\Product;
use App\ProductTag;
use App\Category;
use App\ProductVariant;
use App\ProductImage;
use App\Tag;
use App\Color;
use App\SubcategoryProducts;
use Storage;
use App\Exports\BulkExport;
use App\Imports\BulkImport;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products = Product::with('productVariant.productImage')->get();

        return view('admin.product_list', compact('products'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.add_product', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'cat_name' => 'required',
            'name' => 'required',
            'description' => 'required',
            'p_id' => 'required',
            'p_status' => 'required',
            'title' => 'required',
            't_status' => 'required',

        ]);

        $product_tag = new Tag;


        $products['name'] = $request->name;
        $products['description'] = $request->description;
        $products['p_id'] = $request->p_id;
        $products['p_status'] = $request->p_status;
        $ProductCreate = Product::create($products);
        $insertedId = $ProductCreate->id;

        if($insertedId){
            foreach ($request->cat_name as $val) {
                $product_subcategory = new SubcategoryProducts;
                $product_subcategory->category_id = $val;
                $product_subcategory->product_id = $ProductCreate->id;
                $product_subcategory->save();
            }
        }
        $product_tag->title = $request->title;
        $product_tag->t_status = $request->t_status;
        $product_tag->save();
        $data = array('product_id'=>$insertedId,'tag_id'=>$product_tag->id);
        $productTagAdd = ProductTag::Create($data);

        if($product_tag->id && $request->color_name) {
            foreach ($request->color_name as $key => $value){

                $product_variant = new ProductVariant;
                $saveImage = new ProductImage;

                $product_variant->product_id = $insertedId;
                $product_variant->color_id = $request->color_name[$key];
                $product_variant->size = $request->size[$key];
                $product_variant->product_sku = $request->product_sku[$key];
                $product_variant->product_price = $request->product_price[$key];
                $product_variant->product_compare_price = $request->product_compare_price[$key];
                $product_variant->product_cost = $request->product_cost[$key];
                $product_variant->product_qty_cdmx = $request->product_qty_cdmx[$key];
                $product_variant->product_qty_guadalajara = $request->product_qty_guadalajara[$key];
                $product_variant->product_qty_monterrey = $request->product_qty_monterrey[$key];
                $product_variant->product_qty_puebla = $request->product_qty_puebla[$key];
                if(isset($request->status[$key]) && !is_null($request->status[$key])) {
                    $product_variant->status = $request->status[$key];
                }
                $product_variant->save();


                if (isset($request->images[$key])) {
                    $image = $request->images[$key];

                    $fileExt = $image->getClientOriginalExtension();
                    $renameFileName = strtotime(date('d-m-Y H:i:s')) . '.' . $fileExt;
                    $path = Storage::disk('public')->put('images/' . $renameFileName, file_get_contents($image));
                    $saveImage->images = $renameFileName;
                }

                if (isset($request->images_dual[$key])) {

                    $imaged = $request->images_dual[$key];
                    $fileExt = $imaged->getClientOriginalExtension();
                    $renameFileNames = strtotime(date('d-m-Y H:i:s')) . '.' . $fileExt;
                    $path = Storage::disk('public')->put('images/' . $renameFileNames, file_get_contents($imaged));
                    $saveImage->images_dual = $renameFileNames;
                }
                    $saveImage->product_variant_id = $product_variant->id;

                    $saveImage->save();

            }
        }


        return back()->with('status', 'Product Created Successfully');


    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $pro_variant = ProductVariant::with('productImage','color')->get();
        return view('admin.product_list', ['pro_variant' => $pro_variant]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $product = Product::find($id);
        $categories = Category::all();
        $colors = Color::all();
        $subCatProject = SubcategoryProducts::where('product_id',$id)->get();
        $pro_variant = ProductVariant::with('productImage','color')->where('product_id',$id)->get();
        $tags = ProductTag::with('tags')->where('product_id',$id)->orderBy('id','desc')->first();
       // dd($pro_variant);
        return view('admin.edit_product', compact('pro_variant','categories','colors','product','subCatProject','tags'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatenn(Request $request, $id)
    {
        request()->validate([
            'cat_name' => 'required',
            'name' => 'required',
            'description' => 'required',
            'p_id' => 'required',
            'p_status' => 'required',
            'title' => 'required',
            't_status' => 'required',

        ]);


        $product_tag = new Tag;


        $products['name'] = $request->name;
        $products['description'] = $request->description;
        $products['p_id'] = $request->p_id;
        $products['p_status'] = $request->p_status;
        $ProductCreate = Product::create($products);
        $insertedId = $ProductCreate->id;

        if($insertedId){
            foreach ($request->cat_name as $val) {
                $product_subcategory = new SubcategoryProducts;
                $product_subcategory->category_id = $val;
                $product_subcategory->product_id = $ProductCreate->id;
                $product_subcategory->save();
            }
        }
        $product_tag->title = $request->title;
        $product_tag->t_status = $request->t_status;
        $product_tag->save();
        if($product_tag->id && $request->color_name) {
            foreach ($request->color_name as $key => $value){

                $product_variant = new ProductVariant;
                $saveImage = new ProductImage;

                $product_variant->product_id = $insertedId;
                $product_variant->color_id = $request->color_name[$key];
                $product_variant->size = $request->size[$key];
                $product_variant->product_sku = $request->product_sku[$key];
                $product_variant->product_price = $request->product_price[$key];
                $product_variant->product_compare_price = $request->product_compare_price[$key];
                $product_variant->product_cost = $request->product_cost[$key];
                $product_variant->product_qty_cdmx = $request->product_qty_cdmx[$key];
                $product_variant->product_qty_guadalajara = $request->product_qty_guadalajara[$key];
                $product_variant->product_qty_monterrey = $request->product_qty_monterrey[$key];
                $product_variant->product_qty_puebla = $request->product_qty_puebla[$key];
                if(isset($request->status[$key]) && !is_null($request->status[$key])) {
                    $product_variant->status = $request->status[$key];
                }
                $product_variant->save();


                if (isset($request->images[$key])) {
                    $image      = $request->images[$key];

                    $fileExt = $image->getClientOriginalExtension();
                    $renameFileName = strtotime(date('d-m-Y H:i:s')).'.'.$fileExt;
                    $path = Storage::disk('public')->put('images/'.$renameFileName, file_get_contents($image));


                    $imaged      = $request->images_dual[$key];
                    $fileExt = $imaged->getClientOriginalExtension();
                    $renameFileNames = strtotime(date('d-m-Y H:i:s')).'.'.$fileExt;
                    $path = Storage::disk('public')->put('images/'.$renameFileNames, file_get_contents($imaged));
                    // Save image into database

                    $saveImage->product_variant_id = $product_variant->id;
                    $saveImage->images = $renameFileName;
                    $saveImage->images_dual = $renameFileNames;
                    $saveImage->save();
                }

            }
        }


        return back()->with('status', 'Product Created Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

    }

    public function export(Request $request)
    {
        return Excel::download(new BulkExport, 'bulkData.xlsx');
    }


    public function activeAll(Request $request)
    {

        $ids = $request->ids;

        \DB::table("products")->whereIn('id',explode(",",$ids))->update(['p_status'=>1]);

        return response()->json(['success'=>"status updated successfully."]);
    }

    public function inactiveAll(Request $request)
    {

        $ids = $request->ids;

        \DB::table("products")->whereIn('id',explode(",",$ids))->update(['p_status'=>0]);

        return response()->json(['success'=>"status updated successfully."]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateProject(Request $request)
    {
        request()->validate([
            'cat_name' => 'required',
            'name' => 'required',
            'description' => 'required',
            'p_id' => 'required',
            'p_status' => 'required',
            'title' => 'required',
            't_status' => 'required',

        ]);
//        dd($request);

        $product_tag = new Tag;
        $product = Product::find($request->product_id);

        $product->name = $request->name;
        $product->description = $request->description;
        $product->p_id = $request->p_id;
        $product->p_status = $request->p_status;
        $ProductCreate = $product->save();
        $insertedId = $request->product_id;
        SubcategoryProducts::where('product_id',$insertedId)->delete();
        if($insertedId){

            foreach ($request->cat_name as $val) {
                $product_subcategory = new SubcategoryProducts;
                $product_subcategory->category_id = $val;
                $product_subcategory->product_id = $insertedId;
                $product_subcategory->save();
            }
        }

        $product_tag->title = $request->title;
        $product_tag->t_status = $request->t_status;
        $product_tag->save();
         $pVariants = ProductVariant::where('product_id',$insertedId)->get();
        foreach ($pVariants as $pVariant)
        {
            ProductImage::where('product_variant_id',$pVariant->id)->delete();
        }
        ProductVariant::where('product_id',$insertedId)->delete();

        if($product_tag->id && $request->color_name) {
            foreach ($request->color_name as $key => $value){

                $product_variant = new ProductVariant;
                $saveImage = new ProductImage;

                $product_variant->product_id = $insertedId;
                $product_variant->color_id = $request->color_name[$key];
                $product_variant->size = $request->size[$key];
                $product_variant->product_sku = $request->product_sku[$key];
                $product_variant->product_price = $request->product_price[$key];
                $product_variant->product_compare_price = $request->product_compare_price[$key];
                $product_variant->product_cost = $request->product_cost[$key];
                $product_variant->product_qty_cdmx = $request->product_qty_cdmx[$key];
                $product_variant->product_qty_guadalajara = $request->product_qty_guadalajara[$key];
                $product_variant->product_qty_monterrey = $request->product_qty_monterrey[$key];
                $product_variant->product_qty_puebla = $request->product_qty_puebla[$key];
                if(isset($request->status[$key]) && !is_null($request->status[$key])) {
                    $product_variant->status = $request->status[$key];
                }
                $product_variant->save();
//                dd($request->images_dual[$key]);

                if (isset($request->images[$key])) {
                    $image = $request->images[$key];

                    $fileExt = $image->getClientOriginalExtension();
                    $renameFileName = strtotime(date('d-m-Y H:i:s')) . '.' . $fileExt;
                    $path = Storage::disk('public')->put('images/' . $renameFileName, file_get_contents($image));
                    $saveImage->images = $renameFileName;
                }

                    if (isset($request->images_dual[$key])) {

                        $imaged = $request->images_dual[$key];
                        $fileExt = $imaged->getClientOriginalExtension();
                        $renameFileNames = strtotime(date('d-m-Y H:i:s')) . '.' . $fileExt;
                        $path = Storage::disk('public')->put('images/' . $renameFileNames, file_get_contents($imaged));
                        $saveImage->images_dual = $renameFileNames;
                    }
                    // Save image into database

                    $saveImage->product_variant_id = $product_variant->id;


                    $saveImage->save();


            }
        }


        return back()->with('status', 'Product Created Successfully');

    }


}
