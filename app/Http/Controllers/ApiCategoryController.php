<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CategoryController;
use App\Category;
use App\Family;
use DB;

class ApiCategoryController extends Controller
{
    //
    public function index()
       {
        $categories = Category::select('id','cat_name')->get();
                  return response()->json([
                                    'success' => true,
                                    'message' => 'Product Variant List',
                                    'data' => $categories
                                ], 200);            
        }
 

    public function family_collection()
      {
        $families = Family::select('id','name')->get();
                    return response()->json([
                                      'success' => true,
                                      'message' => 'Category List',
                                      'data' => $families
                                  ], 200);  
      }


    public function category_subcategory_list()
    {
          $families_subcategory = DB::table('categories')->join('family','categories.family_id', '=', 'family.id')->select('family.id','family.name','categories.id','categories.cat_name','categories.images')->get();
          foreach ($families_subcategory  as $key => $value) {
                $filename = asset('storage/images/'.$value->images);
                $image = Array('images' => $filename);
                $value->images = $image;
               // $value->description = strip_tags($value->description);
                }
                return response()->json([
                                  'success' => true,
                                  'message' => 'Category Subcategory List',
                                  'data' => $families_subcategory
                              ], 200);  
    }

     public function subcategory()
    {
          $sub_categories = DB::table('family')->leftjoin('category_subcategory','family.id', '=', 'category_subcategory.family_id')
        ->join('categories', 'category_subcategory.categories_id','=', 'categories.id')
          ->select('category_subcategory.family_id','categories.id','categories.cat_name','categories.images' )
          ->get();
             foreach ($sub_categories  as $key => $value) {
                $filename = asset('storage/images/'.$value->images);
                $image = Array('images' => $filename);
                $value->images = $image;
               // $value->description = strip_tags($value->description);
                }
                return response()->json([
                                  'success' => true,
                                  'message' => 'Category Subcategory List',
                                  'data' => $sub_categories
                              ], 200);  
    }
     
}
