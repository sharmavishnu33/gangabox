<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator, Redirect, Response;
use App\Http\Controllers\ProductVariantController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Product;
use App\ProductTag;
use App\ProductVariant;
use App\ProductImage;
use App\Category;
use App\Family;
use App\SubcategoryProducts;
use App\Tag;
use App\Color;
use Storage;
use DB;

class ApiProductController extends Controller
{

    public function store(Request $request)
    {

        //try {
        // request()->validate([
        //     'name' => 'required',
        //     'description' => 'required',
        //     'p_id' => 'required',
        //     'product_video_link' => 'required',
        //     'p_status' => 'required',
        //     'title' => 'required',
        //     'status' => 'required',
        //     'color_name' => 'required|not_in:0',
        //     'size' => 'required',
        //     'product_sku' => 'required',
        //     'product_price' => 'required',
        //     'product_compare_price' => 'required',
        //     'product_cost' => 'required',
        //     'product_qty_cdmx' => 'required',
        //     'product_qty_guadalajara' => 'required',
        //     'product_qty_monterrey' => 'required',
        //     'product_qty_puebla' => 'required',
        //     'status' => 'required',
        //     'images' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        //    ]);


        $products = new Product;


        $product_tag = new Tag;
        $product_color = new Color;
        $product_variant = new ProductVariant;
        $saveImage = new ProductImage;

        $products->name = $request->name;

        $products->description = $request->description;
        $products->p_id = $request->p_id;
        $products->product_video_link = $request->product_video_link;
        $products->p_status = $request->p_status;
        $products->save();


        $product_tag->title = $request->title;
        $product_tag->status = $request->status;
        $product_tag->save();

        $product_color->color_name = $request->color_name;
        $product_color->save();

        $product_variant->product_id = $products->id;
        $product_variant->color_id = $product_color->id;
        $product_variant->size = $request->size;
        $product_variant->product_sku = $request->product_sku;
        $product_variant->product_price = $request->product_price;
        $product_variant->product_compare_price = $request->product_compare_price;
        $product_variant->product_cost = $request->product_cost;
        $product_variant->product_qty_cdmx = $request->product_qty_cdmx;
        $product_variant->product_qty_guadalajara = $request->product_qty_guadalajara;
        $product_variant->product_qty_monterrey = $request->product_qty_monterrey;
        $product_variant->product_qty_puebla = $request->product_qty_puebla;
        $product_variant->status = $request->status;
        $product_variant->save();

        if ($request->hasFile('images')) {
            $image = $request->file('images');
            $imageName = $request->images->getClientOriginalName();
            Storage::disk('public')->put('images/' . $imageName, file_get_contents($image));

            // Save image into database

            $saveImage->product_variant_id = $product_variant->id;
            $saveImage->images = $imageName;
            $saveImage->save();
        }


        return response()->json([
            'status' => 'success',
            'status_code' => 200,
            // 'data' => $pro_variant,
            'request' => ($request->all()),
            'message' => 'Successfully Added Products',

        ], 200);
        //}catch (Exception $e) {
        return response()->json(['success' => false, 'message' => $e->getMessage()]);
        // }
    }

    public function view()
    {
        $pro_variant = DB::table('products')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->join('products_variants', 'products.id', '=', 'products_variants.product_id')
            ->join('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
            ->select('products.category_id', 'products.name', 'products.description', 'products_variants.product_price', 'products_images.images')
            ->get();

        foreach ($pro_variant as $key => $value) {
            $filename = asset('storage/images/' . $value->images);
            $image = Array('images' => $filename);
            $value->images = $image;
            $value->description = strip_tags($value->description);
        }

        return response()->json([
            'success' => true,
            'message' => 'Category Product List',
            'data' => $pro_variant
        ], 200);
    }


    public function product_variant_list()
    {
        $pro_variant = DB::table('categories')
            ->join('subcategory_products', 'categories.id', '=', 'subcategory_products.category_id')
            ->join('products', 'subcategory_products.product_id', '=', 'products.id')
            // ->select('products.description','subcategory_products.product_id')

            ->get();


        if ($pro_variant) {
            $result = array();
            foreach ($pro_variant as $key => $pvariant) {


                //   array(
                //             //'id' => $pvariant->id,
                //             // 'name' => $pvariant->name,
                //             // 'description' => $pvariant->description,
                //            // 'color_id' => $variant->color_id,
                //             'id' => $pvariant->product_id,
                //             'name' => $pvariant->name,
                //             'color_name' => $variant->color_name,
                //             'size' => $variant->size,
                //             'price' => $variant->product_price,

                // );
                if ($pvariant->product_id) {
                    $variants = DB::table('products')
                        ->leftjoin('products_variants', 'products.id', '=', 'products_variants.product_id')
                        ->join('colors', 'colors.id', '=', 'products_variants.color_id')
                        ->leftjoin('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
                        ->select('products_images.images', 'products_variants.color_id', 'products_variants.size', 'products_variants.product_price', 'products_variants.product_cost', 'products_variants.product_qty_cdmx', 'products_variants.product_qty_guadalajara', 'products_variants.product_qty_puebla', 'products_variants.status',
                            'colors.color_name')->where('product_id', $pvariant->product_id)
//DB::raw('GROUP_CONCAT(products_variants.product_cost) as product_cost'),DB::raw('GROUP_CONCAT(products_variants.product_qty_cdmx) as qty_cdmx'),DB::raw('GROUP_CONCAT(products_variants.product_qty_guadalajara) as qty_guadalajara') ,DB::raw('GROUP_CONCAT(products_variants.product_qty_monterrey) as qty_monterrrey'),DB::raw('GROUP_CONCAT(products_variants.product_qty_puebla) as qty_puebla'),DB::raw('GROUP_CONCAT(products_variants.status) as variants_status'))->groupBy('products_variants.product_id')
                        ->get();
//                dd($variants);

                    if ($variants) {
                        $arr = array();
                        foreach ($variants as $key => $variant) {
//                        dd($variant);
                            if ($variant->color_name && $variant->color_id && $variant->size && $variant->product_price &&
                                $variant->product_cost && $variant->product_qty_cdmx && $variant->product_qty_guadalajara && $variant->product_qty_puebla && $variant->status) {
                                // $pro_color_id = explode(",", $variant->color_id);
                                // $pro_color_name = explode(",", $variant->color_name);
                                // $pro_size = explode(",", $variant->size);
                                // $pro_price = explode(",", $variant->product_price);
                                // $pro_cost = explode(",", $variant->product_cost);
                                // $pro_qty_cdmx = explode(",", $variant->qty_cdmx);
                                // $pro_qty_guadalajara = explode(",", $variant->qty_guadalajara);
                                // $pro_qty_cdmx = explode(",", $variant->qty_cdmx);
                                // $pro_qty_monterrrey = explode(",", $variant->qty_monterrrey);
                                // $pro_qty_puebla = explode(",", $variant->qty_puebla);
                                // $pro_status = explode(",", $variant->variants_status);


                                // foreach ($vari as $key => $value) {
                                $arr[] = array(
                                    //'id' => $pvariant->id,
                                    // 'name' => $pvariant->name,
                                    // 'description' => $pvariant->description,
                                    // 'color_id' => $variant->color_id,
                                    'color_id' => $variant->color_id,
                                    'color_name' => $variant->color_name,
                                    'size' => $variant->size,
                                    'price' => $variant->product_price,
                                    'product_cost' => $variant->product_cost,
                                    'qty_cdmx' => $variant->product_qty_cdmx,
                                    'qty_guadalajara' => $variant->product_qty_guadalajara,
                                    'qty_puebla' => $variant->product_qty_puebla,
                                    'status' => $variant->status,
                                    'image' => \Illuminate\Support\Facades\Storage::disk('public')->url('/images/' . $variant->images),


                                    // 'size' => $pro_size[$key],
                                    // 'product_price' => $pro_price[$key],
                                    // 'product_cost' => $pro_cost[$key],
                                    // 'qty_cdmx' => $pro_qty_cdmx[$key],
                                    // 'qty_guadalajara' => $pro_qty_guadalajara[$key],
                                    // 'qty_monterrrey' => $pro_qty_monterrrey[$key],
                                    // 'qty_puebla' => $pro_qty_puebla[$key],
                                    // 'variants_status' => $pro_status[$key],
                                );

                                //  }
                                $pro_variant = $arr;
                                //dd($arr);

                            }

                        }

                    }

                    $result[] = array(


                        'id' => $pvariant->product_id,
                        'name' => $pvariant->name,
                        'description' => strip_tags(preg_replace('/\s|&nbsp;/', '', $pvariant->description)),
                        'product_variant' => $arr
                    );

                }

            }

        }

        return response()->json([
            'success' => true,
            'message' => 'Product Variant List',
            'data' => $result
        ], 200);


    }

    public function view_category_product_list()
    {
        $families_subcategory = DB::table('family')->leftjoin('category_subcategory', 'family.id', '=', 'category_subcategory.family_id')->leftJoin('categories', 'category_subcategory.categories_id', '=', 'categories.id')->leftJoin('subcategory_products', 'categories.id', '=', 'subcategory_products.category_id')
            ->leftjoin('products', 'subcategory_products.product_id', '=', 'products.id')->join('products_variants', 'products.id', '=', 'products_variants.product_id')
            ->join('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
            ->select('family.id as category_id', 'family.name as category_name', 'category_subcategory.family_id as cs_category_id', 'category_subcategory.categories_id as cs_subcategory_id', 'categories.id as subcat_id', 'categories.cat_name as subcat_name', 'subcategory_products.category_id as sp_subcat_id', 'subcategory_products.product_id as sp_product_id', DB::raw('GROUP_CONCAT(products.name) as productname'), DB::raw('GROUP_CONCAT(products.id) as productid'))->groupBy('subcategory_products.product_id')
            // ->join('categories', 'category_subcategory.categories_id','=', 'categories.id')
            // ->leftjoin('subcategory_products', 'categories.id', '=', 'subcategory_products.category_id')
            // ->join('products', 'subcategory_products.product_id', '=', 'products.id')
            // ->join('products_variants', 'products.id', '=', 'products_variants.product_id')
            //     ->join('products_images','products_variants.id', '=', 'products_images.product_variant_id')
            ->select('family.id as family_id', 'family.name as family_name', 'categories.id as cat_id', 'products.name as product_name', 'products.description', 'products_variants.product_price', 'products_images.images')->get();
        //dd($families_subcategory);


        foreach ($families_subcategory as $key => $value) {
            $filename = asset('storage/images/' . $value->images);
            $image = Array('images' => $filename);
            $value->images = $image;
            $des = $value->description;
            $value->description = strip_tags(preg_replace('/\s|&nbsp;/', '', $des));
        }

        return response()->json([
            'success' => true,
            'message' => 'Family Product List',
            'data' => $families_subcategory
        ], 200);
    }

    public function product_list()
    {
        $subcategory = DB::table('categories')
            ->leftjoin('subcategory_products', 'categories.id', '=', 'subcategory_products.category_id')
            ->join('products', 'subcategory_products.product_id', '=', 'products.id')
            ->join('products_variants', 'products.id', '=', 'products_variants.product_id')
            ->join('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
            ->select('categories.id as category', 'products.name as product_name', 'products.description', 'products_images.images', DB::raw('GROUP_CONCAT(products_variants.product_price) as product_price'))->groupBy('products_variants.product_id')->get();


        foreach ($subcategory as $key => $value) {
            $filename = asset('storage/images/' . $value->images);
            $image = Array('images' => $filename);
            $value->images = $image;
            $des = $value->description;
            $value->description = strip_tags(preg_replace('/\s|&nbsp;/', '', $des));

        }

        return response()->json([
            'success' => true,
            'message' => 'Category Product List',
            'data' => $subcategory
        ], 200);
    }

    public function variantList(Request $request)
    {
        try {

            $rules = [
                'product_id' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {

                $response = ['success' => false, 'message' => 'variant listing failed.'];
            } else {
                $cartList = ProductVariant::where('product_id', $requestData['product_id'])->with('colorData')->get();

                $response = ['success' => true, 'message' => 'Variant list successfully.', 'data' => $cartList];

            }

            return Response::json($response, 200);


        } catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(), 'data' => []], 404);

        }

    }

}
