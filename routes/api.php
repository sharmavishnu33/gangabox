<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();


});
Route::post('/product','ApiProductController@store');
Route::get('/product_list','ApiProductController@view');
Route::get('/variant_list','ApiProductController@product_variant_list');
Route::get('/product_category_list','ApiProductController@view_category_product_list');
Route::get('/subcategory_product_list','ApiProductController@product_list');

Route::get('/family_list','ApiCategoryController@family_collection');
Route::get('/subcategory','ApiCategoryController@category_subcategory_list');
Route::get('/category_list','ApiCategoryController@index');
Route::get('/subcategory_list','ApiCategoryController@subcategory');
Route::get('/list','ApiProductController@variantList');

Route::post('add_cart','ApiCartController@cartAdd')->name('cart.add');
Route::get('list_cart','ApiCartController@cartlist');
Route::post('add_guest_info','ApiCartController@clientInformation')->name('add.guest');
Route::post('edit_cart','ApiCartController@editCart')->name('edit.cart');
Route::post('delete_cart','ApiCartController@deleteCart')->name('delete.cart');
Route::post('add_order_details','ApiCartController@addOrder')->name('add.order.details');


