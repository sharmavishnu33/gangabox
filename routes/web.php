<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', 'AdminController@index');
Route::resource('/product','ProductController');
Route::get('editproject/{id}', 'ProductController@edit')->name('edit-project');
Route::post('update-project', 'ProductController@updateProject')->name('update.project');

Route::get('activeAll', 'ProductController@activeAll');
Route::get('inactiveAll', 'ProductController@inactiveAll');

Route::resource('/AddProduct', 'ProductVariantController');
Route::resource('/product_tag', 'ProductTagController');
Route::match(['get', 'post'], '/store_tags', 'ProductTagController@storeTags');

/*Excel import export*/
Route::get('export', 'ProductController@export')->name('export');

Route::resource('/collection', 'CollectionController');

Route::resource('/categories', 'CategoryController');

Route::resource('/orders', 'OrderController');


