<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('order_details')->insert([
            [
                'product_id' => '1',
                'order_number' => '23421',
                'price' => '556',
                'quantity_qty_cdmx' => '1',
                'product_qty_guadalajara' => '1',
                'product_qty_monterrey' => '1',
                'product_qty_puebla' => '1',
                'discount' => '20',
                'total' => '334',
                'product_sku' => '22',
                'size' => 'S',
                'color' => '2',
                'shipdate' => '',
                'billdate' => '',
            
            ],
           
        ]);
    }
    }
