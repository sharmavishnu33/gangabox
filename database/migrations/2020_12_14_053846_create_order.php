<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_details_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('payment_id');
            $table->unsignedBigInteger('shipper_id');
            $table->timestamp('order_date');
            $table->date('ship_date');
            $table->string('ship_charges');
            $table->timestamp('required_date');
            $table->string('sales_tax');
            $table->string('err_loc');
            $table->string('err_msg');
            $table->string('paid');
            $table->timestamp('payment_date');
            $table->tinyInteger('transaction_status')->default(1);
            $table->timestamp('deleted_at');
            $table->timestamps();
             
            $table->foreign('order_details_id')
            ->references('id')->on('order_details')
            ->onDelete('cascade');

             $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('payment_id')
            ->references('id')->on('payment')
            ->onDelete('cascade');

            $table->foreign('shipper_id')
            ->references('id')->on('shippers')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
