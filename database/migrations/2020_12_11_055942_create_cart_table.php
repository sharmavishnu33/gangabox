<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->id();
            $table->string('product_name')->nullable();
            $table->string('product_price')->nullable();
            $table->string('description')->nullable();
            $table->string('product_image')->nullable();
            $table->string('quantity')->nullable();
            $table->string('product_color')->nullable();
            $table->string('sub_total_price')->nullable();
            $table->string('shipping_price')->nullable();
            $table->string('total_price')->nullable();
            $table->string('device_token')->nullable();
            $table->integer('product_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
