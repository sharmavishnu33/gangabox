<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_variants', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('color_id')->unsigned();
            $table->string('product_sku')->nullable();
            $table->string('product_price')->nullable();
            $table->string('product_compare_price')->nullable();
            $table->string('product_cost')->nullable();
            $table->set('size', ['XS', 'S', 'M', 'L', 'XL', 'XXL', '3XL'])->nullable();
            $table->string('product_qty_cdmx')->nullable();
            $table->string('product_qty_guadalajara')->nullable();;
            $table->string('product_qty_monterrey')->nullable();;
            $table->string('product_qty_puebla')->nullable();;
            $table->tinyInteger('status')->default(1);
            $table->timestamps();

             $table->foreign('product_id')
            ->references('id')->on('products')
            ->onDelete('cascade');

            $table->foreign('color_id')
            ->references('id')->on('colors')
            ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_variants');
    }
}
