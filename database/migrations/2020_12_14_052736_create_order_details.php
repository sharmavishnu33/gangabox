<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_id');
            $table->integer('order_number');
            $table->string('price');
            $table->string('quantity_qty_cdmx');
            $table->string('product_qty_guadalajara');
            $table->string('product_qty_monterrey');
            $table->string('product_qty_puebla');
            $table->string('discount');
            $table->string('total');
            $table->string('product_sku');
            $table->string('size');
            $table->string('color');
            $table->timestamp('shipdate');
            $table->time('billdate');
            $table->timestamps();

             $table->foreign('product_id')
            ->references('id')->on('products')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
