<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
   <!-- Sidebar scroll-->
   <div class="scroll-sidebar">
      <!-- Sidebar navigation-->
      <nav class="sidebar-nav">
         <ul id="sidebarnav">
            <!-- User Profile-->
            <li>
               <!-- User Profile-->
              <!--  <div class="user-profile d-flex no-block dropdown mt-3">
                  <div class="user-pic"><img src="../../assets/images/users/1.jpg" alt="users" class="rounded-circle" width="40" /></div>
                  <div class="user-content hide-menu ml-2">
                     <a href="javascript:void(0)" class="" id="Userdd" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <h5 class="mb-0 user-name font-medium">Steave Jobs <i class="fa fa-angle-down"></i></h5>
                        <span class="op-5 user-email">varun@gmail.com</span>
                     </a>
                     <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Userdd">
                        <a class="dropdown-item" href="javascript:void(0)"><i class="ti-user mr-1 ml-1"></i> My Profile</a>
                        <a class="dropdown-item" href="javascript:void(0)"><i class="ti-wallet mr-1 ml-1"></i> My Balance</a>
                        <a class="dropdown-item" href="javascript:void(0)"><i class="ti-email mr-1 ml-1"></i> Inbox</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void(0)"><i class="ti-settings mr-1 ml-1"></i> Account Setting</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void(0)"><i class="fa fa-power-off mr-1 ml-1"></i> Logout</a>
                     </div>
                  </div>
               </div> -->
               <!-- End User Profile-->
            </li>
            <!-- <li class="p-15 mt-2"><a href="javascript:void(0)" class="btn btn-block create-btn text-white no-block d-flex align-items-center"><i class="fa fa-plus-square"></i> <span class="hide-menu ml-1">Create New</span> </a></li> -->
            <!-- User Profile-->
            <!-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Personal</span></li> -->
            <li class="sidebar-item">
               <!-- <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i data-feather="home" class="feather-icon"></i><span class="hide-menu">Dashboard </span></a> -->
               
               <ul aria-expanded="false" class="collapse  first-level">
                  <li class="sidebar-item"><a href="/dashboard" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> INDEX </span></a></li>
                  <li class="sidebar-item"><a href="/orders" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> ORDERS </span></a></li>
                  <li class="sidebar-item"><a href="" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> ABANDONED CART </span></a></li>

                 <li class="sidebar-item"><a href="/AddProduct" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu">ADD PRODUCTS </span></a>
                  </li>
                  <li class="sidebar-item"><a href="/product" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu">PRODUCT LIST </span></a>
                  </li>
                  <li class="sidebar-item"><a href="/categories" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu">CATEGORY LIST</span></a>
                  </li>
                 <li class="sidebar-item">
               <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i data-feather="sidebar" class="feather-icon"></i><span class="hide-menu">INVENTORY LIST</span></a>
               <ul aria-expanded="false" class="collapse  first-level">
                  <li class="sidebar-item"><a href="" class="sidebar-link"><i class="mdi mdi-view-quilt"></i><span class="hide-menu"> GLOBAL INVENTORY </span></a></li>
                  <li class="sidebar-item"><a href="" class="sidebar-link"><i class="mdi mdi-view-parallel"></i><span class="hide-menu"> PUEBLA </span></a></li>
                  <li class="sidebar-item"><a href="" class="sidebar-link"><i class="mdi mdi-view-day"></i><span class="hide-menu"> GUADALAZARA </span></a></li>
                  <li class="sidebar-item"><a href="" class="sidebar-link"><i class="mdi mdi-view-array"></i><span class="hide-menu"> MONTERREY </span></a></li>
                  <li class="sidebar-item"><a href="" class="sidebar-link"><i class="mdi mdi-view-module"></i><span class="hide-menu"> CDMX </span></a></li>
                  <li class="sidebar-item"><a href="" class="sidebar-link"><i class="mdi mdi-view-quilt"></i><span class="hide-menu"> GLOBAL INVENTORY </span></a></li>

               </ul>
               </li>
                <li class="sidebar-item"><a href="" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> SHIPPING </span></a>
               </li>
                <li class="sidebar-item"><a href="" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> BANNERS </span></a>
               </li>
            
               </ul>
            </li>
          
         </ul>
      </nav>
      <!-- End Sidebar navigation -->
   </div>
   <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->