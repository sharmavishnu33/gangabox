@extends('layouts.master')
@section('content')
@include('layouts.header')
@include('layouts.sidebar')


 <!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- ============================================================== -->
 <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
             <form method="get" action="">
            <div class="row">
                    <!-- Column -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                    <a class="btn waves-effect waves-light btn-secondary" href="/collection/create">CREATE COLLECTION</a>
                    
                   
 

                    </div>
                </div>
            </div>
        </div>
       
        
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">COLLECTION LIST</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->

            <!-- Container fluid  -->
            <!-- ============================================================== -->
          
               
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table product-overview" id="zero_config">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" id="CheckAll" ></th>
                                                <th>IMAGE</th>
                                                <th>NAME</th>
                                                <th>STATUS</th>
                                                <th>SALES BY DATE</th>
                                                <th>% BY THE GLOBAL SALES</th>
                                                
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
            </div> 
            </form>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

        <script>
            $(document).ready(function(){
                $('#t_id').attr('disabled', true);
            });
            $('#selectall').click(function(){
                $("input[type='checkbox']").prop('checked',this.checked);

            });

            function checkSelected()
            {
                var checkboxes = $('[name="selectitem[]"]:checked').length;
                if(checkboxes > 0) {
                    $('#t_id').attr('disabled', false);
                } else {
                    $('#t_id').attr('disabled', true);
                }  
            }
            
            $('.selectitem').click(function(){
                checkSelected();
                
            });

            $('#selectall').click(function(){
                checkSelected();
                
            });

            $('#remove_button').click(function(){
                $(this).val('active');
            });

        </script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('.active_all').on('click', function(e) {


            var allVals = [];
            $(".selectitem:checked").each(function() {
                allVals.push($(this).attr('data-id'));
            });


            if(allVals.length <=0)
            {
                alert("Please select row.");
            }  else {


                var check = confirm("Are you sure you want to Active this Product?");
                if(check == true){


                    var join_selected_values = allVals.join(",");

//alert(join_selected_values);
                    $.ajax({
                        url: $(this).data('url'),
                        type: 'GET',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: 'ids='+join_selected_values,
                        success: function (data) {
                            if (data['success']) {
                                $(".sub_chk:checked").each(function() {
                                    
                                });
                                window.location.reload(true); 
                                 $('#output_code').html(data['success']);
                               
                            } 
                        },
                        
                    });


                
                }
            }
        });


        


    });
</script>
<script>
  
  $(document).ready(function () {
        $("#CheckAll").click(function () {
            $(".selectitem").attr('checked', this.checked);
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {

        $('.inactive_all').on('click', function(e) {


            var allVals = [];
            $(".selectitem:checked").each(function() {
                allVals.push($(this).attr('data-id'));
            });


            if(allVals.length <=0)
            {
                alert("Please select row.");
            }  else {


                var check = confirm("Are you sure you want to In-Active this Product?");
                if(check == true){


                    var join_selected_values = allVals.join(",");


                    $.ajax({
                        url: $(this).data('url'),
                        type: 'GET',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: 'ids='+join_selected_values,
                        success: function (data) {
                            if (data['success']) {
                                $(".selectitem:checked").each(function() {
                                    
                                });
                                window.location.reload(true); 
                                 $('#output_code').html(data['success']);
                               
                            } 
                        },
                        
                    });
                }
            }
        });
     });
</script>
@include('layouts.footer')
@endsection

