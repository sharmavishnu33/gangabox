@extends('layouts.master')
@section('content')
@include('layouts.header')
@include('layouts.sidebar')


 <!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- ============================================================== -->
 <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
        <form method="get" action="">
          <div class="row">
                    <!-- Column -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                      <a class="btn waves-effect waves-light btn-secondary" href="/categories/create">CREATE CATEGORY</a>
                    </div>
                </div>
            </div>
        </div>
       
        
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">CATEGORY LIST</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->

            <!-- Container fluid  -->
            <!-- ============================================================== -->
          
               
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table product-overview" id="zero_config">
                                        <thead>
                                            <tr>
                                                <th>FAMILY NAME</th>
                                                <th>CATEGORY NAME</th>
                                                <th>DESCRIPTION</th>
                                                <th>STATUS</th>
                                              <!--   <th>ACTION</th> -->
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($categories as $category)
                                            <tr>
                                                <td>{{$category->name}}</td>
                                                <td>{{$category->cat_name}}</td>
                                                <td>{{$category->cat_desc}}</td>
                                                <td> @if($category->status ==1)
                                                        Active
                                                    @else
                                                        In-active
                                                    @endif </td>
                                                <!--  <td>
                                                    <a href="categories/{{$category->id}}/edit" class="btn btn-success" data-toggle="tooltip" title="edit" >
                                            <i class="fa fa-edit"></i>
                                        </a></td> -->
                                        
                                            </tr>
                                            @endforeach
                                          </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
            </div> 
            </form>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>


@include('layouts.footer')
@endsection

