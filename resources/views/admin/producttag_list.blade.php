@extends('layouts.master')
@section('content')
@include('layouts.header')
@include('layouts.sidebar')


 <!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- ============================================================== -->
 <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
           
           
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Product Tag List</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->

            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->

                     @if (session('status'))
                        <div class="alert alert-success alert-dismissable">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           {{ session('status') }}
                        </div>
                    @endif
                <form method="post" action="{{url('/store_tags')}}">
                    @csrf
                    <div class="row">
                        <!-- Column -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table product-overview" id="zero_config">
                                            <thead>
                                                <tr>
                                                    
                                                    <th>TAG NAME</th>
                                                    <th>ACTION</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($tags as $tag)

                                                <tr>
                                                    
                                                    <td>{{$tag->title}}</td>
                                                     <th><input type="checkbox" id="CheckAll" ></th>
                                                    <!-- <td><button type="submit" class="btn waves-effect waves-light btn-success tag_btn" data-tag_id = "{{$tag->id}}">Add Tag</button>&nbsp;<button type="submit" class="btn waves-effect waves-light btn-danger tag_btn" data-tag_id = "{{$tag->id}}">Remove Tag</button></td> -->
                                                   
                                                </tr>
                                              @endforeach
                                              
                                        </table>
                                        @if($selected_tags)
                                            @foreach($selected_tags as $tag)
                                            <input type="hidden" name="products[]" value="{{ $tag }}">
                                            @endforeach
                                        @endif
                                        <input type="hidden" id="tid" name="tid" value="">
                                        @if($remove_pro)
                                            <input type="hidden" id="remove_button" name="remove_button" value="{{$remove_pro}}">
                                        @endif
                                    </div>
                                    <button type="submit" class="btn waves-effect waves-light btn-danger tag_btn" >Submit</button>
                                            </tbody>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                    </div>  
                </form>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
        <script>
            $('.tag_btn').click(function(){
                var tag_id = $(this).data('tag_id');
                $('#tid').val(tag_id);
            })
        </script>
@include('layouts.footer')
@endsection