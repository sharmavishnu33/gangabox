@extends('layouts.master')
@section('content')
@include('layouts.header')
@include('layouts.sidebar')
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title"> CREATE COLLECTION</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
            </div>
           
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                     @if (session('status'))
                        <div class="alert alert-success alert-dismissable">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           {{ session('status') }}
                        </div>
                    @endif

                    @if (session('success'))
                        <div class="alert alert-success alert-dismissable">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           {{ session('success') }}
                        </div>
                    @endif

            @if (session('error'))
                <div class="alert alert-danger alert-dismissable">
                   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                   {{ session('error') }}
                </div>
            @endif   
                    <div class="col-12">
                        <div class="card">
                            <form class="form-horizontal" action="{{url('')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="card-body">
                                    <h4 class="card-title">ADD COLLECTION</h4>
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-3 text-right control-label col-form-label">NAME:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="name" name="name" placeholder="NAME" value="{{ old('name') }}" >
                                              @if($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                                        </div>
                                    </div>

                                    <div class="form-group row {{ $errors->has('images') ? ' has-error' : '' }}">
                                        <label for="" class="col-sm-3 text-right control-label col-form-label">CATEGORY IMAGE:</label>
                                        <div class="col-sm-9">
                                            <input id="file" type="file"  name="images" value="{{ old('images') }}" />
                                Supported Formats[JPEG,PNG]</br>
                                @if ($errors->has('images'))
                                    <span class="text-danger">
                                        {{ $errors->first('images') }}
                                    </span>
                                @endif
                                    <span class="help-block">
                                        <strong></strong>
                                    </span>
                                     
                              
                                        </div>
                                    </div>

                                    <div class="form-group row {{ $errors->has('images') ? ' has-error' : '' }}">
                                        <label for="" class="col-sm-3 text-right control-label col-form-label">BANNER IMAGE:</label>
                                        <div class="col-sm-9">
                                            <input id="file" type="file"  name="images" value="{{ old('images') }}" />
                                Supported Formats[JPEG,PNG]</br>
                                @if ($errors->has('images'))
                                    <span class="text-danger">
                                        {{ $errors->first('images') }}
                                    </span>
                                @endif
                                    <span class="help-block">
                                        <strong></strong>
                                    </span>
                                     
                              
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('images') ? ' has-error' : '' }}">
                                        <label for="" class="col-sm-3 text-right control-label col-form-label">BANNER IMAGE:</label>
                                        <div class="col-sm-9">
                                            <input id="file" type="file"  name="images" value="{{ old('images') }}" />
                                Supported Formats[JPEG,PNG]</br>
                                @if ($errors->has('images'))
                                    <span class="text-danger">
                                        {{ $errors->first('images') }}
                                    </span>
                                @endif
                                    <span class="help-block">
                                        <strong></strong>
                                    </span>
                                     
                              
                                        </div>
                                    </div>
                                    <h4 class="card-title">CONDITIONS</h4>
                            
                                    <div class="form-group row">
                                        <label for="product_id" class="col-sm-3 text-right control-label col-form-label">BY PRODUCT TAG:</label>
                                        <div class="col-sm-9">
                                            <input type="product_id" class="form-control" id="product_id" name="p_id" placeholder="PRODUCT ID"  value="{{ old('p_id') }}">
                @if($errors->has('p_id'))
                    <span class="text-danger">{{ $errors->first('p_id') }}</span>
                @endif

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-3 text-right control-label col-form-label">BY PRICE <=:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="" name="product_video_link" placeholder="PRODUCT VIDEO LINK" value="{{ old('product_video_link') }}">
                                            @if($errors->has('product_video_link'))
                    <span class="text-danger">{{ $errors->first('product_video_link') }}</span>
                @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-3 text-right control-label col-form-label">BY PRICE >=:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="title" name="title" placeholder="PPRODUCT TAGS"  value="{{ old('title') }}">
                                             @if($errors->has('title'))
                    <span class="text-danger">{{ $errors->first('title') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                     <div class="form-group row {{ $errors->has('images') ? ' has-error' : '' }}">
                                        <label for="" class="col-sm-3 text-right control-label col-form-label">PRODUCT ORDER LIST GOOGLE SHEETS:</label>
                                        <div class="col-sm-9">
                                            <input id="file" type="file"  name="images" value="{{ old('images') }}" />
                               
                                @if ($errors->has('images'))
                                    <span class="text-danger">
                                        {{ $errors->first('images') }}
                                    </span>
                                @endif
                                    <span class="help-block">
                                        <strong></strong>
                                    </span>
                                     
                              
                                        </div>
                                    </div>

                                    
                                </div>
                                <hr>   
                                <div class="card-body">
                                    <div class="form-group mb-0 text-right">
                                        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                    </div>
                                </div>
                            </form>
                               
                        </div>
                    </div>
                </div>
                <!-- End Row -->
  
            </div>
            
@include('layouts.footer')
@endsection

