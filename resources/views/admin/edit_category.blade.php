@extends('layouts.master')
@section('content')
@include('layouts.header')
@include('layouts.sidebar')
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title"> EDIT CATEGORIES</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
            </div>
           
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                     @if (session('status'))
                        <div class="alert alert-success alert-dismissable">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           {{ session('status') }}
                        </div>
                    @endif

                    @if (session('success'))
                        <div class="alert alert-success alert-dismissable">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           {{ session('success') }}
                        </div>
                    @endif

            @if (session('error'))
                <div class="alert alert-danger alert-dismissable">
                   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                   {{ session('error') }}
                </div>
            @endif   
                    <div class="col-12">
                        <div class="card">
                            <form class="form-horizontal" action="/categories/{{$categories->id}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="card-body">
                                    <h4 class="card-title">EDIT CATEGORIES</h4>
                                     <div class="form-group row">
                                        <label for="" class="col-sm-3 text-right control-label col-form-label">Family List:</label>
                                        <div class="col-sm-9">
                                            <select class="form-control @error('name') is-invalid @enderror" name="name">
                              <option disabled selected>{{__('Select family of the category')}}</option>
                              @foreach($categories as $category)
                              <option value="{{$category->name}}" >{{$categories->name}}</option>
                              @endforeach
                           </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label for="cat_name" class="col-sm-3 text-right control-label col-form-label">NAME:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" value="{{$categories->cat_name}}" id="cat_name" name="cat_name" placeholder="NAME" value="{{ old('cat_name') }}" >
                                              @if($errors->has('cat_name'))
                    <span class="text-danger">{{ $errors->first('cat_name') }}</span>
                @endif
                                        </div>
                                    </div>
                            
                                    <div class="form-group row">
                                        <label for="cat_desc" class="col-sm-3 text-right control-label col-form-label">CATEGORY DESCRIPTION:</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" id="cat_desc" name="cat_desc" placeholder="DESCRIPTION"  value="{{ old('cat_desc') }}">{{$categories->cat_desc}}</textarea>
                @if($errors->has('cat_desc'))
                    <span class="text-danger">{{ $errors->first('cat_desc') }}</span>
                @endif

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="" class="col-sm-3 text-right control-label col-form-label">CATEGORY STATUS:</label>
                                        <div class="col-sm-9">
                                            <select value="{{$categories->status}}" id="status" name="status" class="form-control @error('status') is-invalid @enderror">
        <option value="{{$categories->status}}" selected>{{$categories->status}}</option>
          <option value="1" {{old('status') == 'Yes' ? 'selected' : ''}}>Yes</option>
       <option value="0" {{old('status') == 'No' ? 'selected' : ''}}>No</option>       
    </select>
      @if($errors->has('status'))
                    <span class="text-danger">{{ $errors->first('status') }}</span>
        @endif
                                        </div>
                                    </div>
                                    
                                    

                                <hr>   
                                <div class="card-body">
                                    <div class="form-group mb-0 text-right">
                                        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                    </div>
                                </div>
                            </form>
                               
                        </div>
                    </div>
                </div>
                <!-- End Row -->
  
            </div>
            
@include('layouts.footer')
@endsection

